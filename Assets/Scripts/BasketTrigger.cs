﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BasketTrigger : MonoBehaviour {

    public GameObject gameManager;
    public GameObject spawner;
    public GameObject[] itemsBonus;
    public Text[] str;

    AudioSource audio;
    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Money")
        {
            if (gameManager.GetComponent<Main>().CheckStartGame())
            {
                gameManager.GetComponent<Main>().score += other.gameObject.GetComponent<MoneyParams>().GetMoneyScore();
                str[0].text = gameManager.GetComponent<Main>().score.ToString();
                str[1].text = gameManager.GetComponent<Main>().score.ToString();
                Destroy(other.gameObject);
                int index = 0;
                switch (other.gameObject.GetComponent<MoneyParams>().index)
                {
                    case 1:
                        index = 0;
                        break;
                    case 2:
                        index = 1;
                        break;
                    case 3:
                        index = 1;
                        break;
                    case 4:
                        index = 3;
                        break;
                    case 5:
                        index = 4;
                        break;
                    case 6:
                        index = 5;
                        break;
                    default:
                        index = 0;
                        break;
                }
                GameObject ob = Instantiate(itemsBonus[index], other.gameObject.transform.position, Quaternion.LookRotation(other.gameObject.transform.position)) as GameObject;
                //ob.transform.rotation = Quaternion.LookRotation(other.gameObject.transform.position);
                Destroy(ob, 1.5f);
                audio.Play();
            }
        }
    }
}
