﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraScript : MonoBehaviour {

    public GameObject[] panels;
    //public GameObject box;
    public GameObject[] firstSceneComp;
    public GameObject[] secondSceneComp;
    //public GameObject particles;
    public GameObject spawnPositions;
    public GameObject secondCamera;

    float speed = 3; // скорость - (1 == 1 сек) (0.1 == 10 сек) 
    float timer = 0;
    string mode = "up";
    public string environment = "dream";

    bool change = false;
    public bool camMove = false;
    Vector3 camPos;
    Quaternion camRot;
    // Use this for initialization
    void Start () {
        camPos = secondCamera.transform.position;
        camRot = secondCamera.transform.rotation;
    }
	
	// Update is called once per frame
	void Update () {
        if (camMove)
        {
            secondCamera.gameObject.transform.position = transform.position;
            secondCamera.gameObject.transform.rotation = transform.rotation;
        }
        if (change)
        {
            if(mode == "up")
            {
                if (timer <= 10)
                {
                    timer += Time.deltaTime * speed;
                    panels[0].GetComponent<Image>().material.SetFloat("_Size", timer);
                    panels[1].GetComponent<Image>().material.SetFloat("_Size", timer);
                }
                else
                {
                    if (environment == "dream")
                        GoToDream();
                    else if (environment == "start")
                        GoBack();
                    mode = "down";
                }
            }else if(mode == "down")
            {
                if (timer > 0)
                {
                    timer -= Time.deltaTime * speed;
                    panels[0].GetComponent<Image>().material.SetFloat("_Size", timer);
                    panels[1].GetComponent<Image>().material.SetFloat("_Size", timer);
                }
                else
                {
                    change = false;
                    mode = "up";
                    panels[0].SetActive(false);
                    panels[1].SetActive(false);
                }
            }
        }
    }
    void GoToDream()
    {
        foreach (GameObject ob in firstSceneComp)
        {
            ob.SetActive(false);
        }
        foreach (GameObject ob in secondSceneComp)
        {
            ob.SetActive(true);
        }
        gameObject.GetComponent<Camera>().clearFlags = CameraClearFlags.Skybox;
        secondCamera.GetComponent<Camera>().clearFlags = CameraClearFlags.Skybox;
    }
    IEnumerator WaitSomeSeconds(string _environment, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        environment = _environment;
        change = true;
        panels[0].SetActive(true);
        panels[1].SetActive(true);
    }
    public void GoBackCamPos()
    {
        camMove = false;
        secondCamera.transform.position = camPos;
        secondCamera.transform.rotation = camRot;
    }
    public void Transfer(string _environment, float seconds)
    {
        StartCoroutine(WaitSomeSeconds(_environment, seconds));
    }
    public void GoBack()
    {
        foreach (GameObject ob in firstSceneComp)
        {
            ob.SetActive(true);
        }
        foreach (GameObject ob in secondSceneComp)
        {
            ob.SetActive(false);
        }
        
        gameObject.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
        secondCamera.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
    }
    public void Money(bool val)
    {
        spawnPositions.SetActive(val);
        if(val)
            spawnPositions.GetComponent<SpawnItems>().StartSpawn();
    }
}
