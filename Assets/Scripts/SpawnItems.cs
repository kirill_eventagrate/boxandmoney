﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnItems : MonoBehaviour {
    public GameObject[] items;
    public GameObject[] spawnPoints;
    
    // Use this for initialization

    List<Balls> balls = new List<Balls>();

    public void StartSpawn() {
        balls.Add(new Balls(100, 261, 1));
        balls.Add(new Balls(100, 261, 1));
        balls.Add(new Balls(100, 261, 1));
        balls.Add(new Balls(100, 261, 1));
        balls.Add(new Balls(100, 261, 1));
        balls.Add(new Balls(500, 52, 2));
        balls.Add(new Balls(500, 52, 2));
        balls.Add(new Balls(500, 52, 2));
        balls.Add(new Balls(500, 52, 2));
        balls.Add(new Balls(1000, 52, 3));
        balls.Add(new Balls(1000, 52, 3));
        balls.Add(new Balls(1000, 52, 3));
        balls.Add(new Balls(25000, 12, 4));
        balls.Add(new Balls(50000, 4, 5));
        balls.Add(new Balls(100000, 1, 6));
        StartCoroutine(InitContent());
    }
    IEnumerator InitContent()
    {
        float waitTime = UnityEngine.Random.Range(0, 2.5f);
        yield return new WaitForSeconds(waitTime);
        if (balls.Count > 0)
        {
            int spawnIndex = Convert.ToInt32(UnityEngine.Random.Range(0, spawnPoints.Length));
            int itemIndex = Convert.ToInt32(UnityEngine.Random.Range(0, balls.Count - 1));
            try
            {
                GameObject ob = Instantiate(items[balls[itemIndex].GetType() - 1], spawnPoints[spawnIndex].transform.position, Quaternion.identity) as GameObject;
                ob.GetComponent<MoneyParams>().bonus = balls[itemIndex].GetBonus();
                ob.GetComponent<MoneyParams>().value = balls[itemIndex].GetValue();
                ob.GetComponent<MoneyParams>().index = balls[itemIndex].GetType();
            }
            catch
            {
                Debug.Log("itemIndex: " + itemIndex + "; count: " + balls.Count);
            }
            Repeat();
        }
        
    }

    void Repeat()
    {
        StartCoroutine(InitContent());
    }
    
    public int GetCountBalls()
    {
        return balls.Count;
    }
    // Update is called once per frame
    void Update () {
        
    }
}

class Balls
{
    int value;
    int bonus;
    int type;
    public Balls(int _value, int _bonus, int _type)
    {
        value = _value;
        bonus = _bonus;
        type = _type;
    }
    public int GetValue() { return value; }
    public int GetBonus() { return bonus; }
    public int GetType() { return type; }
}
