﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animations : MonoBehaviour {

    Animator anim;
    Vector3 startPosition;
    Quaternion startRotation;
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        startPosition = transform.position;
        startRotation = transform.rotation;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.S))
        {
            
        }
        
    }
    public void GoAnimation()
    {
        anim.SetBool("Go", true);
    }
    public void GoToStartPosition()
    {
        transform.position = startPosition;
        transform.rotation = startRotation;
    }

}
