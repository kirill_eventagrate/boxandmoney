﻿using System.Collections;
using System.Collections.Generic;
using SharpOSC;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Threading;

public class Main : MonoBehaviour {
    /*public GameObject canvas;
    public GameObject logoCanvas;*/
    public GameObject LeaderBoardCanvas;
    public GameObject tabloCanvas;
    public GameObject finalCanvas;
    public GameObject cameraObj;
    public GameObject boxUp;
    

    public Text[] lableTime;
    public Text[] scoreValue;
    public Text leaderList;
    public Text finalScoreText;

    public int score;
    public float gameTime = 120;

    private int maxSphere = 800;
    private bool startGame = false;
    private bool showLeaderList = false;
    private bool showResult = false;
    private bool ready = false;
    private User user = new User();

    float speed = 1; // скорость - (1 == 1 сек) (0.1 == 10 сек) 
    
    float timer;

    OscMessage messageReceived;
    UDPListener listener;
    List<User> usersList = new List<User>();


    void Awake()
    {
        if (!File.Exists("leaderList.csv"))
        {
            File.Create("leaderList.csv");
        }
        StreamReader sw = new StreamReader(@"leaderList.csv");
        while (!sw.EndOfStream)
        {
            var line = sw.ReadLine();
            var values = line.Split(';');
            usersList.Add(new User(values[0], values[1], values[2], int.Parse(values[3])));
        }
    }
    // Use this for initialization
    void Start () {
        listener = new UDPListener(7000);
        messageReceived = null;
    }

    void FixedUpdate()
    {
        messageReceived = (OscMessage)listener.Receive();
        if (messageReceived != null)
        {
            if (messageReceived.Address == "/nextMode")
            {
                boxUp.GetComponent<Animations>().GoAnimation();
                cameraObj.GetComponent<CameraScript>().Transfer("dream", 1.2f);
            }
            if (messageReceived.Address == "/newUser")
            {
                if (messageReceived.Arguments[0].ToString() == "Start")
                {
                    Debug.Log(messageReceived.Arguments[1].ToString());
                    
                    score = 0;
                    timer = gameTime;
                    lableTime[0].text = timer.ToString();
                    lableTime[1].text = timer.ToString();
                    scoreValue[0].text = "0";
                    scoreValue[1].text = "0";
                    var line = messageReceived.Arguments[1].ToString();
                    var values = line.Split(';');
                    user = new User(values[0], values[1], values[2]);
                    cameraObj.GetComponent<CameraScript>().Money(true);
                    cameraObj.GetComponent<CameraScript>().camMove = true;
                    tabloCanvas.SetActive(true);
                    startGame = true;
                }
            }
            if (messageReceived.Address == "/showLeaderList")
            {
                if (messageReceived.Arguments[0].ToString() == "Show")
                {
                    ShowLeaderList();
                }
            }
        }

        if (startGame)
        {
            if (timer >= 1)
            {
                timer -= Time.deltaTime * speed;
                lableTime[0].text = ((int)timer).ToString();
                lableTime[1].text = ((int)timer).ToString();
            }
            else
            {
                EndGame();
            }
        }
        if (showLeaderList)
        {
            if (timer >= 1)
            {
                timer -= Time.deltaTime * speed;
            }
            else
            {
                LeaderBoardCanvas.SetActive(false);
            }
        }
        if (showResult)
        {
            if (timer >= 1)
            {
                timer -= Time.deltaTime * speed;
            }
            else
            {
                finalCanvas.SetActive(false);
            }
        }

    }
    public bool CheckStartGame()
    {
        return startGame;
    }
    public void EndGame()
    {
        startGame = false;
        cameraObj.GetComponent<CameraScript>().Money(false);
        user.SetScore(score);
        SaveLeader(user);
        boxUp.GetComponent<Animations>().GoToStartPosition();
        cameraObj.GetComponent<CameraScript>().Transfer("start", 0f);
        cameraObj.GetComponent<CameraScript>().camMove = false;
        cameraObj.GetComponent<CameraScript>().GoBackCamPos();
    }
    private void SaveLeader(User user)
    {
        usersList.Add(user);
        usersList.Sort(delegate (User us1, User us2)
        { return us2.score.CompareTo(us1.score); });
        //ShowLeaderList();
        StartCoroutine(ShowResult());
    }
    IEnumerator ShowResult()
    {
        yield return new WaitForSeconds(4f);
        if (score >= 1300000)
            finalScoreText.text = "CONGRATULATIONS!\nYOU WIN 1.3 MILLION!";
        else
            finalScoreText.text = "OH GOOD!\nYOU GOT " + score.ToString() + " MONEY\nBUT YOU COULD HAVE WIN 1.3 MILLION";
        
        //finalScoreText.text = score.ToString();
        tabloCanvas.SetActive(false);
        finalCanvas.SetActive(true);
        timer = 30;
        showResult = true;
    }
    private void ShowLeaderList()
    {
        string text = "";
        foreach (User us in usersList)
        {
            text = text + us.name + ": " + us.score.ToString() + "\n";
        }
        leaderList.text = text;
        LeaderBoardCanvas.SetActive(true);
        showLeaderList = true;
        timer = 10;
        //cameraObj.GetComponent<Camera>().depth = -1;
    }
    void OnDestroy()
    {
        File.WriteAllText(@"leaderList.csv", string.Empty);
        StreamWriter sw = new StreamWriter(@"leaderList.csv");
        foreach (User us in usersList)
        {
            sw.WriteLine(us.number + ";" + us.name + ";" + us.email + ";" + us.score);
        }
        sw.Close();
    }
}
class User
{
    public string name;
    public string email;
    public string number;
    public int score;

    public User() { }
    public User(string _number, string _name, string _email)
    {
        number = _number;
        name = _name;
        email = _email;
    }
    public User(string _number, string _name, string _email, int _score)
    {
        number = _number;
        name = _name;
        email = _email;
        score = _score;
    }
    public void SetScore(int _score)
    {
        score = _score;
    }
}