﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyParams : MonoBehaviour {

    public int bonus;
    public int value;
    public int index;
    // 
    Quaternion originRotation;
    float angle;

    void Start()
    {
        originRotation = transform.rotation;
    }
    void FixedUpdate()
    {
        angle++;
        Quaternion rotationY = Quaternion.AngleAxis(angle, Vector3.up);
        Quaternion rotationX = Quaternion.AngleAxis(angle, Vector3.right);
        transform.rotation = originRotation * rotationY * rotationX;
    }
    public int GetMoneyScore()
    {
        return value * bonus;
    }
}
